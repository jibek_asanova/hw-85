const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};

    if (req.query.album) {
      query.album = req.query.album;
    }
    const tracks = await Track
      .find(query)
      .populate('album', 'title releasedYear')
      .sort({number: 1})
    ;
    res.send(tracks);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

router.post('/', async (req, res) => {
  try {
    const trackData = {
      number: req.body.number,
      title: req.body.title,
      album: req.body.album || null,
      duration: req.body.duration || null
    };


    const track = new Track(trackData);
    await track.save();
    res.send(track);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const track = await Track.findOne({_id: req.params.id});

    track.published = !track.published;
    await track.save();
    res.send(track);
  } catch (error) {
    res.status(400).send(error)
  }
});

router.delete('/:id', auth, permit('admin'),async (req, res) => {
  try {
    const track = await Track.findByIdAndDelete(req.params.id);

    if(track) {
      res.send(`track ${track.title} removed`);
    } else {
      res.status(404).send({error: 'Track not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
