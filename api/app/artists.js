const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const artists = await Artist.find();
    res.send(artists);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});


router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
  try {
    const artistData = {
      title: req.body.title,
      description: req.body.description || null
    };

    if (req.file) {
      artistData.image = 'uploads/' + req.file.filename
    }

    const artist = new Artist(artistData);
    await artist.save();
    res.send(artist);

  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const artist = await Artist.findOne({_id: req.params.id});


    artist.published = !artist.published;

    await artist.save();
    res.send(artist);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const artist = await Artist.findByIdAndDelete(req.params.id);

    if (artist) {
      res.send(`artist ${artist.title} removed`);
    } else {
      res.status(404).send({error: 'Artist not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
