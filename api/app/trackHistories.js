const express = require('express');
const Track = require("../models/Track");
const TrackHistory = require("../models/TrackHistory");
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
  if (!req.body.track) {
    return res.status(400).send('Data not valid');
  }
  try {
    const track = await Track.findById(req.body.track);

    if (!track) {
      return res.status(404).send({error: 'Track not found'});
    }
    const trackHistoryData = {
      user: req.user._id,
      track: track,
      datetime: new Date(),
    };

    const trackHistory = new TrackHistory(trackHistoryData);

    await trackHistory.save();
    res.send(trackHistory);

  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const tracks = await TrackHistory
      .find({user: req.user._id})
      .populate({
        path: 'track',
        select: 'title',
        populate: {
          path: 'album',
          select: 'artist',
          populate: {
            path: 'artist',
            select: 'title'
          }
        }
      })
      .sort({datetime: -1})
    ;
    res.send(tracks);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

module.exports = router;