const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};

    if (req.query.artist) {
      query.artist = req.query.artist;
    }
    const albums = await Album
      .find(query)
      .populate('artist', 'title description')
      .sort({releasedYear: 1});
    res.send(albums);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});


router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const albumData = {
      title: req.body.title,
      artist: req.body.artist || null,
      releasedYear: req.body.releasedYear || null
    };

    if (req.file) {
      albumData.image = 'uploads/' + req.file.filename
    }
    const album = new Album(albumData);

    await album.save();
    res.send(album);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const album = await Album.findOne({_id: req.params.id});

    album.published = !album.published;
    await album.save();
    res.send(album);
  } catch (error) {
    res.status(400).send(error)
  }
});

router.delete('/:id', auth, permit('admin'),async (req, res) => {
  try {
    const album = await Album.findByIdAndDelete(req.params.id);

    if(album) {
      res.send(`album ${album.title} removed`);
    } else {
      res.status(404).send({error: 'Album not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
