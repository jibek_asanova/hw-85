const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");


const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [Rihanna, Beyonce, Taylor] = await Artist.create({
      title: 'Rihanna',
      description: 'Singer',
      image: 'fixtures/rihana.jpg',
      published: true,
    }, {
      title: 'Beyonce',
      description: 'Singer',
      image: 'fixtures/Beyonce.jpg',
      published: false,

    }, {
      title: 'Taylor Swift',
      description: 'Singer',
      image: 'fixtures/taylorSwift.png',
      published: true,
    }
  );

  const [Album1, Album2, Album3, Album4] = await Album.create({
    title: 'Album1',
    artist: Rihanna,
    releasedYear: '2002',
    image: 'fixtures/rihanna_album.png',
    published: true,

  }, {
    title: 'Album2',
    artist: Beyonce,
    releasedYear: '1999',
    image: 'fixtures/Beyonce_album.png',
    published: false,

  }, {
    title: 'Album3',
    artist: Taylor,
    releasedYear: '2015',
    image: 'fixtures/taylor_album.jpg',
    published: true,

  }, {
    title: 'Album4',
    artist: Rihanna,
    releasedYear: '2012',
    image: 'fixtures/loud.jpeg',
    published: false,

  });

  await Track.create({
    number: 1,
    title: 'bad boy',
    album: Album1,
    duration: '3min',
    published: true,

  }, {
    number: 2,
    title: 'hello',
    album: Album2,
    duration: '3min',
    published: false,

  }, {
    number: 3,
    title: 'trouble',
    album: Album3,
    duration: '3min',
    published: false,

  }, {
    number: 4,
    title: 'diamonds',
    album: Album4,
    duration: '3min',
    published: true,

  }, {
    number: 5,
    title: 'uber',
    album: Album1,
    duration: '3min',
    published: true,

  });

  await User.create({
      email: 'jibek@gmail.com',
      password: '123',
      token: nanoid(),
      role: 'admin',
      displayName: 'Jibek',
    }, {
      email: 'bob@gmail.com',
      password: '555',
      token: nanoid(),
      role: 'user',
      displayName: 'Bob',
    },
    {
      email: 'kate@gmail.com',
      password: '254',
      token: nanoid(),
      role: 'admin',
      displayName: 'Kate',
    },
    {
      email: 'dan@gmail.com',
      password: '255',
      token: nanoid(),
      role: 'user',
      displayName: 'Dan'
    }
  )
  await mongoose.connection.close();

};

run().catch(console.error)