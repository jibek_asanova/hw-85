const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');


const AlbumSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  artist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  releasedYear: String,
  image: String,
  published: {
    type: Boolean,
    default: false
  }

});


AlbumSchema.plugin(idvalidator);

const Album = mongoose.model('Album', AlbumSchema);


module.exports = Album;