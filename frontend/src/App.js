import Layout from "./components/UI/Layout/Layout";
import {Switch, Route, Redirect} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";
import {useSelector} from "react-redux";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists}/>
                <ProtectedRoute
                    path="/artists/new"
                    component={AddArtist}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <ProtectedRoute
                    path="/albums/new"
                    component={AddAlbum}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <ProtectedRoute
                    path="/tracks/new"
                    component={AddTrack}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <Route path="/albums" component={Albums}/>
                <Route path="/tracks" component={Tracks}/>
                <ProtectedRoute
                    path="/trackHistory"
                    component={TrackHistory}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>

    );
};

export default App;
