import {Box, Grid, Paper, Typography} from "@material-ui/core";
import PropTypes from 'prop-types';


const TrackHistoryItem = ({artistTitle, title, datetime}) => {
    return (
        <Grid item container direction="column">
            <Paper component={Box} p={2} spacing={2}>
                <Typography variant="h4">Artist: {artistTitle}</Typography>
                <Typography variant="h5">Title: {title}</Typography>
                <Typography variant="subtitle2">DateTime: {datetime}</Typography>
            </Paper>
        </Grid>

    );
};

TrackHistoryItem.propTypes = {
    artistTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    datetime: PropTypes.string.isRequired
}

export default TrackHistoryItem;