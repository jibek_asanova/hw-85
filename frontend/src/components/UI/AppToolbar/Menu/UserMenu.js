import React, {useState} from 'react';
import {Avatar, Button, Grid, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import {Link} from "react-router-dom";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
            <Grid container>
                <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                    Hello, {user.displayName}!
                </Button>
                <Avatar src={user.avatarImage} style={{alignSelf: 'center'}}/>
            </Grid>

            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >

                <MenuItem><Button component={Link} to="/trackHistory" color="inherit">Track History</Button></MenuItem>
                <MenuItem><Button color="inherit" component={Link} to="/artists/new">Add artist</Button></MenuItem>
                <MenuItem><Button color="inherit" component={Link} to="/albums/new">Add album</Button></MenuItem>
                <MenuItem><Button color="inherit" component={Link} to="/tracks/new">Add track</Button></MenuItem>
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;