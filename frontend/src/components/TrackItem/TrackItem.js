import {Button, Card, CardContent, CardHeader, Grid, makeStyles, Typography,} from "@material-ui/core";
import PropTypes from 'prop-types';
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {saveTrackHistories} from "../../store/actions/trackHistoryActions";
import {deleteTrack, publishTrack} from "../../store/actions/tracksActions";


const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
    },
    button: {
        marginTop: '10px'
    }
})

const TrackItem = ({id, title, number, duration, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const location = useLocation().search;


    const onSubmit = async () => {
        await dispatch(saveTrackHistories(id));
    };


    return (
        <>
            {user && user.role === 'admin' ?
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                Number: #{number}
                            </Typography>
                        </CardContent>
                        <CardHeader title={title}/>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                Duration: {duration}
                            </Typography>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={onSubmit}
                                className={classes.button}
                            >Add to history</Button>
                            <Button onClick={() => dispatch(deleteTrack(id))}>Delete</Button>
                            {published ? <Button onClick={() => dispatch(publishTrack(id, location))}>Unpublished</Button> :
                                <Button onClick={() => dispatch(publishTrack(id, location))}>Publish</Button>}
                        </CardContent>
                    </Card>
                </Grid> : published ?
                    <Grid item xs={12} sm={6} md={6} lg={4}>
                        <Card className={classes.card}>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    Number: #{number}
                                </Typography>
                            </CardContent>
                            <CardHeader title={title}/>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    Duration: {duration}
                                </Typography>
                                {user && <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={onSubmit}
                                    className={classes.button}
                                >Add to history</Button>}
                            </CardContent>
                        </Card>
                    </Grid> : null}
        </>

    );
};

TrackItem.propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string
}

export default TrackItem;