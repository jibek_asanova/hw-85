import {
    Button,
    Card, CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid, IconButton,
    makeStyles, Typography,
} from "@material-ui/core";
import PropTypes from 'prop-types';
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import {Link, useLocation} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, publishAlbum} from "../../store/actions/albumsActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    button: {
        marginTop: '10px'
    }
})

const AlbumItem = ({title, id, image, releasedYear, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const location = useLocation().search;

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    return (
        <>
            {user && user.role === 'admin' ?
            <Grid item xs={12} sm={6} md={6} lg={4}>
                <Card className={classes.card}>
                    <CardHeader title={title}/>
                    <CardMedia
                        image={cardImage}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            released year: {releasedYear}
                        </Typography>

                    </CardContent>
                    <CardActions>
                        <IconButton component={Link} to={`/tracks?album=${id}`}>
                            <ArrowForwardIcon />
                        </IconButton>
                        <Button onClick={() => dispatch(deleteAlbum(id))}>Delete</Button>
                        {published ? <Button onClick={() => dispatch(publishAlbum(id, location))}>Unpublished</Button> :
                            <Button onClick={() => dispatch(publishAlbum(id, location))}>Publish</Button>}
                    </CardActions>
                </Card>
            </Grid> : published ?
                    <Grid item xs={12} sm={6} md={6} lg={4}>
                        <Card className={classes.card}>
                            <CardHeader title={title}/>
                            <CardMedia
                                image={cardImage}
                                title={title}
                                className={classes.media}
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    released year: {releasedYear}
                                </Typography>

                            </CardContent>
                            <CardActions>
                                <IconButton component={Link} to={`/tracks?album=${id}`}>
                                    <ArrowForwardIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid> : null }
        </>

    );
};

AlbumItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    image: PropTypes.string
}

export default AlbumItem;