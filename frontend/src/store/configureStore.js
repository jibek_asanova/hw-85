import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import usersReducer from "./reducers/usersReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import artistsReducer from "./reducers/artistsReducer";
import albumsReducer from "./reducers/albumsReducer";
import tracksReducer from "./reducers/tracksReducer";
import trackHistoryReducer from "./reducers/trackHistoryReducer";
import axiosApi from "../axiosApi";

const rootReducer = combineReducers({
    'artists': artistsReducer,
    'albums': albumsReducer,
    'tracks': tracksReducer,
    'users': usersReducer,
    'trackHistory': trackHistoryReducer
});


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk)
    ));

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        },
    });
});

axiosApi.interceptors.response.use(res => res, e => {
    if(!e.response) {
        e.response = {data: {global: 'No internet'}};
    }

    throw e;
})

export default store;