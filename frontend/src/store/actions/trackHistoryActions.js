import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";

export const FETCH_TRACK_HISTORIES_REQUEST = 'FETCH_TRACK_HISTORIES_REQUEST';
export const FETCH_TRACK_HISTORIES_SUCCESS = 'FETCH_TRACK_HISTORIES_SUCCESS';
export const FETCH_TRACK_HISTORIES_FAILURE = 'FETCH_TRACK_HISTORIES_FAILURE';

export const SAVE_TRACK_REQUEST = 'SAVE_TRACK_REQUEST';
export const SAVE_TRACK_SUCCESS = 'SAVE_TRACK_SUCCESS';
export const SAVE_TRACK_FAILURE = 'SAVE_TRACK_FAILURE';

export const saveTrackRequest = () => ({type: SAVE_TRACK_REQUEST});
export const saveTrackSuccess = () => ({type: SAVE_TRACK_SUCCESS});
export const saveTrackFailure = () => ({type: SAVE_TRACK_FAILURE});

export const fetchTrackHistoriesRequest = () => ({type: FETCH_TRACK_HISTORIES_REQUEST});
export const fetchTrackHistoriesSuccess = trackHistories => ({type: FETCH_TRACK_HISTORIES_SUCCESS, payload: trackHistories});
export const fetchTrackHistoriesFailure = () => ({type: FETCH_TRACK_HISTORIES_FAILURE});

export const fetchTrackHistories = () => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(fetchTrackHistoriesRequest());
            const response = await axiosApi.get('/track_history', {headers});
            dispatch(fetchTrackHistoriesSuccess(response.data));
        } catch (error) {
            if(error.response.status === 401) {
                toast.warning('You need to login')
            } else {
                toast.error('Could not fetch tracks!', {
                    theme: 'colored',
                })
            }
            dispatch(fetchTrackHistoriesFailure());
        }
    };
};

export const saveTrackHistories = (id) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(saveTrackRequest());
            const response = await axiosApi.post('/track_history', {track: id}, {headers});
            dispatch(saveTrackSuccess(response.data));
        } catch (error) {
            if(error.response.status === 401) {
                toast.warning('You need to login')
            } else {
                toast.error('Could not fetch tracks!', {
                    theme: 'colored',
                })
            }
            dispatch(saveTrackFailure());
        }
    };
};