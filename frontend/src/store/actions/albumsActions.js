import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const FETCH_ALL_ALBUMS_REQUEST = 'FETCH_ALL_ALBUMS_REQUEST';
export const FETCH_ALL_ALBUMS_SUCCESS = 'FETCH_ALL_ALBUMS_SUCCESS';
export const FETCH_ALL_ALBUMS_FAILURE = 'FETCH_ALL_ALBUMS_FAILURE';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
export const PUBLISH_ALBUM_FAILURE = 'PUBLISH_ALBUM_FAILURE';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';

export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, payload: albums});
export const fetchAlbumsFailure = () => ({type: FETCH_ALBUMS_FAILURE});

export const fetchAllAlbumsRequest = () => ({type: FETCH_ALL_ALBUMS_REQUEST});
export const fetchAllAlbumsSuccess = allAlbums => ({type: FETCH_ALL_ALBUMS_SUCCESS, payload: allAlbums});
export const fetchAllAlbumsFailure = () => ({type: FETCH_ALL_ALBUMS_FAILURE});

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumFailure = (error) => ({type: CREATE_ALBUM_FAILURE, payload: error});

export const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
export const publishAlbumSuccess = () => ({type: PUBLISH_ALBUM_SUCCESS});
export const publishAlbumFailure = () => ({type: PUBLISH_ALBUM_FAILURE});

export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = albumId => ({type: DELETE_ALBUM_SUCCESS, payload: albumId});
export const deleteAlbumFailure = () => ({type: DELETE_ALBUM_FAILURE});

export const fetchArtistAlbums = location => {
    return async dispatch => {
        try {
            dispatch(fetchAlbumsRequest());
            const response = await axiosApi.get(`/albums${location}`);
            dispatch(fetchAlbumsSuccess(response.data));
        } catch (error) {
            if (error.response && error.response.data) {
                toast.error(`error: ${error.response.data.error}`);
            } else {
                dispatch(fetchAlbumsFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    };
};
export const fetchAllAlbums = () => {
    return async dispatch => {
        try {
            dispatch(fetchAllAlbumsRequest());
            const response = await axiosApi.get('/albums');
            dispatch(fetchAllAlbumsSuccess(response.data));
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchAllAlbumsFailure(error.response.data));
                toast.error(`error: ${error.response.data.error}`);
            } else {
                dispatch(fetchAllAlbumsFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};


export const createAlbum = albumData => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(createAlbumRequest());
            await axiosApi.post('/albums', albumData, {headers});
            dispatch(createAlbumSuccess());
            toast.success('Album Created!');
            dispatch(historyPush('/'));
        } catch (e) {
            dispatch(createAlbumFailure(e.response.data));
            toast.error('Could not create album')
        }
    };
}

export const publishAlbum = (id, location) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(publishAlbumRequest());
            await axiosApi.put('/albums/' + id, null, {headers});
            dispatch(publishAlbumSuccess());
            dispatch(fetchArtistAlbums(location));
        } catch (e) {
            dispatch(publishAlbumFailure(e.response.data));
            toast.error('Could not publish');
        }
    }
};

export const deleteAlbum = (id) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(deleteAlbumRequest());
            await axiosApi.delete('albums/' + id, {headers});
            dispatch(deleteAlbumSuccess(id));
            toast.success('Album deleted');
        } catch (error) {
            if(error.response && error.response.data) {
                dispatch(deleteAlbumFailure());
                toast.warning(error.response.data.error)
            } else {
                dispatch(deleteAlbumFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};



