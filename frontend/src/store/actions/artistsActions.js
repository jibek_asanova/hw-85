import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const PUBLISH_ARTIST_REQUEST = 'PUBLISH_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'PUBLISH_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_FAILURE = 'PUBLISH_ARTIST_FAILURE';

export const CREATE_ARTIST_REQUEST = 'CREATE_ARTIST_REQUEST';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const CREATE_ARTIST_FAILURE = 'CREATE_ARTIST_FAILURE';

export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, payload: artists});
export const fetchArtistsFailure = () => ({type: FETCH_ARTISTS_FAILURE});

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const createArtistFailure = (error) => ({type: CREATE_ARTIST_FAILURE, payload: error});

export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = () => ({type: PUBLISH_ARTIST_SUCCESS});
export const publishArtistFailure = () => ({type: PUBLISH_ARTIST_FAILURE});

export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = artistId => ({type: DELETE_ARTIST_SUCCESS, payload: artistId});
export const deleteArtistFailure = () => ({type: DELETE_ARTIST_FAILURE});

export const fetchArtists = () => {
    return async dispatch => {
        try {
            dispatch(fetchArtistsRequest());
            const response = await axiosApi.get('/artists');
            dispatch(fetchArtistsSuccess(response.data));
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchArtistsFailure(error.response.data));
                toast.error(`error: ${error.response.data.error}`);
            } else {
                dispatch(fetchArtistsFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};

export const createArtist = artistData => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(createArtistRequest());
            await axiosApi.post('/artists', artistData, {headers});
            dispatch(createArtistSuccess());
            toast.success('Artist Created!');
            dispatch(historyPush('/'));

        } catch (e) {
            dispatch(createArtistFailure(e.response.data));
            toast.error('Could not create artist')
        }
    };
}

export const publishArtist = (id) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(publishArtistRequest());
            await axiosApi.put('/artists/' + id, null, {headers});
            dispatch(publishArtistSuccess());
            dispatch(fetchArtists());
        } catch (e) {
            dispatch(publishArtistFailure(e.response.data));
            toast.error('Could not create artist')
        }
    }
};

export const deleteArtist = (id) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(deleteArtistRequest());
            await axiosApi.delete('artists/' + id, {headers});
            dispatch(deleteArtistSuccess(id));
            toast.success('Artist deleted');
        } catch (error) {
            if(error.response && error.response.data) {
                dispatch(deleteArtistFailure());
                toast.warning(error.response.data.error)
            } else {
                dispatch(deleteArtistFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};
