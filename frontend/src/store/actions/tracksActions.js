import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";


export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';

export const CREATE_TRACK_REQUEST = 'CREATE_TRACK_REQUEST';
export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const CREATE_TRACK_FAILURE = 'CREATE_TRACK_FAILURE';

export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_FAILURE = 'PUBLISH_TRACK_FAILURE';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_FAILURE = 'DELETE_TRACK_FAILURE';

export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, payload: tracks});
export const fetchTracksFailure = () => ({type: FETCH_TRACKS_FAILURE});

export const createTrackRequest = () => ({type: CREATE_TRACK_REQUEST});
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = (error) => ({type: CREATE_TRACK_FAILURE, payload: error});

export const publishTrackRequest = () => ({type: PUBLISH_TRACK_REQUEST});
export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});
export const publishTrackFailure = () => ({type: PUBLISH_TRACK_FAILURE});

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = trackId => ({type: DELETE_TRACK_SUCCESS, payload: trackId});
export const deleteTrackFailure = () => ({type: DELETE_TRACK_FAILURE});

export const fetchAlbumsTracks = location => {
    return async (dispatch) => {
        try {
            dispatch(fetchTracksRequest());
            const response = await axiosApi.get(`/tracks${location}`);
            dispatch(fetchTracksSuccess(response.data));
        } catch (error) {
            if (error.response && error.response.data) {
                toast.error(`error: ${error.response.data.error}`);
            } else {
                dispatch(fetchTracksFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    };
};


export const createTrack = trackData => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(createTrackRequest());
            await axiosApi.post('/tracks', trackData, {headers});
            dispatch(createTrackSuccess());
            toast.success('Track Created!');
            dispatch(historyPush('/'));
        } catch (e) {
            dispatch(createTrackFailure(e.response.data));
            toast.error('Could not create track')
        }
    };
};

export const publishTrack = (id, location) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(publishTrackRequest());
            await axiosApi.put('/tracks/' + id, null, {headers});
            dispatch(publishTrackSuccess());
            dispatch(fetchAlbumsTracks(location));
        } catch (e) {
            dispatch(publishTrackFailure(e.response.data));
            toast.error('Could not publish');
        }
    }
};

export const deleteTrack = (id) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(deleteTrackRequest());
            await axiosApi.delete('tracks/' + id, {headers});
            dispatch(deleteTrackSuccess(id));
            toast.success('Track deleted');
        } catch (error) {
            if(error.response && error.response.data) {
                dispatch(deleteTrackFailure());
                toast.warning(error.response.data.error)
            } else {
                dispatch(deleteTrackFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};