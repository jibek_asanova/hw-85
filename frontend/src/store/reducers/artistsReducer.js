import {
    CREATE_ARTIST_FAILURE,
    CREATE_ARTIST_REQUEST,
    CREATE_ARTIST_SUCCESS, DELETE_ARTIST_SUCCESS,
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS
} from "../actions/artistsActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    artists: [],
    createArtistLoading: false,
    createArtistError: null
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ARTISTS_SUCCESS:
            return {...state,  fetchLoading: false, artists: action.payload};
        case FETCH_ARTISTS_FAILURE:
            return {...state, fetchLoading: false};
        case CREATE_ARTIST_REQUEST:
            return {...state, createArtisLoading: true};
        case CREATE_ARTIST_SUCCESS:
            return {...state, createArtistLoading: false, createArtistError: null};
        case CREATE_ARTIST_FAILURE:
            return {...state, createArtistLoading: false, createArtistError: action.payload}
        case DELETE_ARTIST_SUCCESS:
            return {...state, artists: state.artists.filter(artist => artist._id !== action.payload)}
        default:
            return state;
    }
};

export default artistsReducer;