import {
    FETCH_TRACK_HISTORIES_FAILURE,
    FETCH_TRACK_HISTORIES_REQUEST,
    FETCH_TRACK_HISTORIES_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
    fetchLoading: false,
    trackHistories: [],
};

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACK_HISTORIES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_TRACK_HISTORIES_SUCCESS:
            return {...state,  fetchLoading: false, trackHistories: action.payload};
        case FETCH_TRACK_HISTORIES_FAILURE:
            return {...state, fetchLoading: false};
        default:
            return state;
    }
};

export default trackHistoryReducer;