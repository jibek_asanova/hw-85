import {
    CREATE_ALBUM_FAILURE,
    CREATE_ALBUM_REQUEST, CREATE_ALBUM_SUCCESS, DELETE_ALBUM_SUCCESS,
    FETCH_ALBUMS_FAILURE,
    FETCH_ALBUMS_REQUEST,
    FETCH_ALBUMS_SUCCESS,
    FETCH_ALL_ALBUMS_SUCCESS
} from "../actions/albumsActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    albums: [],
    allAlbums: [],
    createAlbumLoading: false,
    createAlbumError: null
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ALBUMS_SUCCESS:
            return {...state,  fetchLoading: false, albums: action.payload};
        case FETCH_ALBUMS_FAILURE:
            return {...state, fetchLoading: false};
        case FETCH_ALL_ALBUMS_SUCCESS:
            return {...state,  fetchLoading: false, allAlbums: action.payload};
        case CREATE_ALBUM_REQUEST:
            return {...state, createAlbumLoading: true};
        case CREATE_ALBUM_SUCCESS:
            return {...state, createAlbumLoading: false, createAlbumError: null};
        case CREATE_ALBUM_FAILURE:
            return {...state, createAlbumLoading: false, createAlbumError: action.payload}
        case DELETE_ALBUM_SUCCESS:
            return {...state, albums: state.albums.filter(album => album._id !== action.payload)}
        default:
            return state;
    }
};

export default albumsReducer;