import {
    CREATE_TRACK_FAILURE,
    CREATE_TRACK_REQUEST,
    CREATE_TRACK_SUCCESS, DELETE_TRACK_SUCCESS,
    FETCH_TRACKS_FAILURE,
    FETCH_TRACKS_REQUEST,
    FETCH_TRACKS_SUCCESS
} from "../actions/tracksActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    tracks: [],
    createTrackLoading: false,
    createTrackError: null
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_TRACKS_SUCCESS:
            return {...state,  fetchLoading: false, tracks: action.payload};
        case FETCH_TRACKS_FAILURE:
            return {...state, fetchLoading: false};
        case CREATE_TRACK_REQUEST:
            return {...state, createTrackLoading: true};
        case CREATE_TRACK_SUCCESS:
            return {...state, createTrackLoading: false, createTrackError: null};
        case CREATE_TRACK_FAILURE:
            return {...state, createTrackLoading: false, createTrackError: action.payload}
        case DELETE_TRACK_SUCCESS:
            return {...state, tracks: state.tracks.filter(track => track._id !== action.payload)}
        default:
            return state;
    }
};

export default tracksReducer;