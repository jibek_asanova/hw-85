import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {fetchArtistAlbums} from "../../store/actions/albumsActions";
import AlbumItem from "../../components/AlbumItem/AlbumItem";
import {useLocation} from "react-router-dom";

const Albums = () => {
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const fetchLoading = useSelector(state => state.albums.fetchLoading);
    const location = useLocation();


    useEffect(() => {
        dispatch(fetchArtistAlbums(location.search));
    }, [dispatch, location.search]);

    if(albums.length === 0) {
        return <h1>No albums yet</h1>
    }

    return albums &&(
        <Grid container direction="column" spacing={2}>
            <Grid item container direction="column">
                <Grid item>
                    <Typography variant="h4">{albums[0].artist.title}'s albums</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : albums.map(album => (
                        <AlbumItem
                            key={album._id}
                            id={album._id}
                            title={album.title}
                            image={album.image}
                            releasedYear={album.releasedYear}
                            published={album.published}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Albums;