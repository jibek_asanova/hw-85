import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import ArtistItem from "../../components/ArtistItem/ArtistItem";

const Artists = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const fetchLoading = useSelector(state => state.artists.fetchLoading);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Artists</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : artists.map(artist => (
                        <ArtistItem
                            key={artist._id}
                            id={artist._id}
                            title={artist.title}
                            image={artist.image}
                            published={artist.published}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Artists;