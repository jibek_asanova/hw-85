import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {fetchAlbumsTracks} from "../../store/actions/tracksActions";
import TrackItem from "../../components/TrackItem/TrackItem";
import {useLocation} from "react-router-dom";

const Tracks = () => {
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    const albums = useSelector(state => state.albums.albums);
    const fetchLoading = useSelector(state => state.tracks.fetchLoading);
    const location = useLocation();


    useEffect(() => {
        dispatch(fetchAlbumsTracks(location.search));
    }, [dispatch, location.search]);


    if (tracks.length === 0 || albums.length === 0) {
        return <h1>No tracks yet</h1>
    }

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container direction="column">
                <Grid item>
                    <Typography variant="h4">Artist: {albums[0].artist.title}</Typography>
                </Grid>
                <Grid item>
                    <Typography variant="h4">Album:{tracks[0].album.title}</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : tracks.map(track => (
                        <TrackItem
                            key={track._id}
                            id={track._id}
                            title={track.title}
                            number={track.number}
                            duration={track.duration}
                            published={track.published}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Tracks;