import React, {useEffect, useState} from 'react';
import {makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import Grid from "@material-ui/core/Grid";
import {fetchAllAlbums} from "../../store/actions/albumsActions";
import {createTrack} from "../../store/actions/tracksActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));
const AddTrack = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.allAlbums);
    const error = useSelector(state => state.tracks.createTrackError);
    const loading = useSelector(state => state.tracks.createTrackLoading)

    const [state, setState] = useState({
        number: "",
        title: "",
        album: "",
        duration: ""
    });

    useEffect(() => {
        dispatch(fetchAllAlbums());
    }, [dispatch, albums]);

    const submitFormHandler = async e => {
        e.preventDefault();
        await dispatch(createTrack({...state}))
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    }


    return (
        <>
            <Typography variant="h4">Add Track</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
                noValidate
            >
                <FormElement
                    label="Number"
                    name="number"
                    value={state.number}
                    onChange={inputChangeHandler}
                    required
                    error={getFieldError('number')}
                />
                <FormElement
                    select
                    options={albums}
                    label="Album"
                    name="album"
                    value={state.album}
                    onChange={inputChangeHandler}
                    required
                    error={getFieldError('album')}
                />
                <FormElement
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    required
                    error={getFieldError('title')}
                />

                <FormElement
                    label="Duration"
                    name="duration"
                    value={state.duration}
                    onChange={inputChangeHandler}
                />
                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default AddTrack;