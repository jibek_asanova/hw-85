import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistories} from "../../store/actions/trackHistoryActions";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import TrackHistoryItem from "../../components/TrackHistoryItem/TrackHistoryItem";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const trackHistories = useSelector(state => state.trackHistory.trackHistories);

    const fetchLoading = useSelector(state => state.trackHistory.fetchLoading);

    useEffect(() => {
        dispatch(fetchTrackHistories());
    }, [dispatch]);

    if (trackHistories.length === 0) {
        return <h1>You haven't listened to any tracks yet</h1>
    }

    return (
        <div>
            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">Track History</Typography>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid item container direction="row" spacing={1}>
                        {fetchLoading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : trackHistories.map(track => (
                            <TrackHistoryItem
                                key={track._id}
                                title={track.track.title}
                                artistTitle={track.track.album.artist.title}
                                datetime={track.datetime}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
};

export default TrackHistory;