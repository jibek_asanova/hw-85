import React, {useState} from 'react';
import {makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {createArtist} from "../../store/actions/artistsActions";
import FormElement from "../../components/UI/Form/FormElement";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));
const AddArtist = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const error = useSelector(state => state.artists.createArtistError);
    const loading = useSelector(state => state.artists.createArtistLoading)
    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
    });

    const submitFormHandler = async e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        await dispatch(createArtist(formData))

        // onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    }

    return (
        <>
            <Typography variant="h4">Add Artist</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
                noValidate
            >
                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />
                <FormElement
                    multiline
                    rows={4}
                    label="Description"
                    name="description"
                    value={state.description}
                    onChange={inputChangeHandler}
                />

                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default AddArtist;